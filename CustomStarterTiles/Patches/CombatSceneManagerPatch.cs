﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using TileEnums;
using UnityEngine;

namespace CustomStarterTiles.Patches
{
    static class CombatSceneManagerPatch
    {
        [HarmonyPatch(typeof(CombatSceneManager), "BeginRun", MethodType.Normal)]
        static class CombatSceneManager_BeginRun_Patch
        {
            static void Prefix()
            {
                try
                {
                    var skillEnums = ModModule.SettingsConfig.Skills.Select(x => PatchUtility.SkillIdToEnum(x));
                    List<Skill> list = (from go in Array.ConvertAll<System.Object, GameObject>(Resources.LoadAll("Skills"), (System.Object skill) => (GameObject)skill)
                                       select go.GetComponent<Skill>()).ToList<Skill>();
                    foreach (var skillEnum in skillEnums)
                    {
                        var skill = list.Find(x => x.SkillEnum == skillEnum);
                        SkillsManager.Instance.PickUpSkill(UnityEngine.Object.Instantiate<Skill>(skill, SkillsManager.Instance.transform).GetComponent<Skill>());

                    }
                }
                catch (Exception e)
                {
                    ModModule.Mod.Logger.Error(e.InnerException.ToString());
                }
            }

            static void Postfix()
            {
                try
                {
                    TilesManager.Instance.hand.TCC.DestroyAllTiles();
                    TilesManager.Instance.Deck = new List<Tile>();
                    TilesManager.Instance.hand.Initialize(ModModule.SettingsConfig.DeckSize);
                    TilesManager.Instance.hand.Resize(ModModule.SettingsConfig.DeckSize);

                    if (ModModule.SettingsConfig.IsRandomized)
                    {
                        var attackList = PatchUtility.GetRandomizedDeck();

                        foreach (AttackEnum attackEnum in attackList)
                        {
                            Tile tile = TilesFactory.Instance.Create(attackEnum, 3, AttackEffectEnum.None, TileEffectEnum.None);
                            TilesManager.Instance.TakeTile(tile, false);
                            tile.TileContainer.TeleportTileInContainer();
                            tile.Interactable = false;
                        }
                    }
                    else
                    {
                        var attackSettingsList = PatchUtility.GetCustomDeck();

                        foreach (TileSettings tileSettings in attackSettingsList)
                        {
                            Tile tile = TilesFactory.Instance.Create(tileSettings.AttackValue, tileSettings.Level, PatchUtility.IsDamagingTile(tileSettings.AttackValue) ? tileSettings.Enchantment : AttackEffectEnum.None, tileSettings.IsFreePlay ? TileEffectEnum.FreePlay : TileEffectEnum.None);
                            if (tileSettings.DamageValue != -2 && PatchUtility.IsDamagingTile(tileSettings.AttackValue))
                            {
                                tile.Attack.Value = tileSettings.DamageValue;
                                tile.Attack.BaseValue = tileSettings.DamageValue;
                            }
                            if (tileSettings.CooldownValue != -1)
                            {
                                tile.Attack.Cooldown = tileSettings.CooldownValue;
                                tile.CooldownCharge = tileSettings.CooldownValue;
                            }
                            tile.Initialize(tile.Attack);

                            TilesManager.Instance.TakeTile(tile, false);
                            tile.TileContainer.TeleportTileInContainer();
                            tile.Interactable = false;
                        }
                    }
                }
                catch (Exception e)
                {
                    ModModule.Mod.Logger.Error(e.InnerException.ToString());
                }
            }
        }
    }
}
