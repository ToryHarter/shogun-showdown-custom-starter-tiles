﻿using SkillEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using TileEnums;

namespace CustomStarterTiles.Patches
{
    public static class PatchUtility
    {
        public static List<AttackEnum> GetRandomizedDeck()
        {
            var attacks = (Enum.GetValues(typeof(AttackEnum)) as AttackEnum[]).ToList();

            var attacksToRemove = new[] { AttackEnum.Shield, AttackEnum.Summon, AttackEnum.ShieldAllied, AttackEnum.Volley, AttackEnum.CopycatMirror, AttackEnum.Maku };
            attacks = attacks.Except(attacksToRemove).ToList();

            if (ModModule.SettingsConfig.OnlyObtainableTiles)
            {
                var nonObtainableTiles = new[] { AttackEnum.Bomb, AttackEnum.Chakram, AttackEnum.Nagiboku, AttackEnum.CorruptedBarrage, AttackEnum.Barricade, AttackEnum.CorruptedWaveL2R, AttackEnum.CorruptedWaveR2L, AttackEnum.Hookblade, AttackEnum.Thorns, AttackEnum.BossSwap };
                attacks = attacks.Except(nonObtainableTiles).ToList();
            }

            var random = new Random();

            List<AttackEnum> attackList;
            do
            {
                attackList = new List<AttackEnum>();

                for (var i = 0; i < ModModule.SettingsConfig.DeckSize; i++)
                {
                    attackList.Add(attacks.ElementAt(random.Next(attacks.Count)));
                }
            } while ((!ModModule.SettingsConfig.AllowDuplicates && ContainsDuplicates(attackList)) || (ModModule.SettingsConfig.ForceDamagingTile && !ContainsDamagingTile(attackList)));

            return attackList;
        }

        private static bool ContainsDuplicates(List<AttackEnum> attacks)
        {
            return attacks.Distinct().Count() != ModModule.SettingsConfig.DeckSize;
        }

        public static bool ContainsDamagingTile(List<AttackEnum> attacks)
        {
            var damagingTiles = new List<AttackEnum>
                {
                    AttackEnum.Katana, AttackEnum.Arrow, AttackEnum.Spear, AttackEnum.Bo, AttackEnum.Lightning, AttackEnum.Swirl, AttackEnum.DragonPunch,
                    AttackEnum.GrapplingHook, AttackEnum.TwinTessen, AttackEnum.Trap, AttackEnum.Bomb, AttackEnum.Charge, AttackEnum.BackCharge,
                    AttackEnum.ShadowDash, AttackEnum.SmokeBomb, AttackEnum.BackSmokeBomb, AttackEnum.Nagiboku, AttackEnum.Shuriken, AttackEnum.Chakram,
                    AttackEnum.EarthImpale, AttackEnum.BackStrike, AttackEnum.ShadowKama, AttackEnum.Tetsubo, AttackEnum.Kunai, AttackEnum.BladeOfPatience,
                    AttackEnum.Crossbow, AttackEnum.Tanegashima, AttackEnum.CorruptedBarrage, AttackEnum.ScarStrike, AttackEnum.MeteorHammer,
                    AttackEnum.Kaitenryuken, AttackEnum.BackShadowDash, AttackEnum.Sai, AttackEnum.BlazingSuisei, AttackEnum.Mon, AttackEnum.Thorns,
                    AttackEnum.CorruptedWaveL2R, AttackEnum.CorruptedWaveR2L, AttackEnum.Hookblade
                };

            foreach (var attack in attacks)
            {
                if (damagingTiles.Contains(attack)) return true;
            }

            return false;
        }

        public static List<TileSettings> GetCustomDeck()
        {
            var customDeck = new List<TileSettings>
                {
                    ModModule.SettingsConfig.Tile1,
                    ModModule.SettingsConfig.Tile2,
                    ModModule.SettingsConfig.Tile3,
                    ModModule.SettingsConfig.Tile4,
                    ModModule.SettingsConfig.Tile5,
                    ModModule.SettingsConfig.Tile6,
                    ModModule.SettingsConfig.Tile7,
                    ModModule.SettingsConfig.Tile8,
                };

            customDeck.ForEach(x =>
            {
                x.AttackValue = SelectableTilesToAttackEnum(x.Tile);
                x.DamageValue = DamageToInt(x.Damage);
                x.CooldownValue = CooldownToInt(x.Cooldown);
            });

            return customDeck.Take(ModModule.SettingsConfig.DeckSize).ToList();
        }

        private static AttackEnum SelectableTilesToAttackEnum(SelectableTiles tile)
        {
            switch (tile)
            {
                case SelectableTiles.Katana: return AttackEnum.Katana;
                case SelectableTiles.Arrow: return AttackEnum.Arrow;
                case SelectableTiles.Spear: return AttackEnum.Spear;
                case SelectableTiles.Bo: return AttackEnum.Bo;
                case SelectableTiles.Lightning: return AttackEnum.Lightning;
                case SelectableTiles.Swirl: return AttackEnum.Swirl;
                case SelectableTiles.DragonPunch: return AttackEnum.DragonPunch;
                case SelectableTiles.GrapplingHook: return AttackEnum.GrapplingHook;
                case SelectableTiles.TwinTessen: return AttackEnum.TwinTessen;
                case SelectableTiles.Trap: return AttackEnum.Trap;
                case SelectableTiles.Bomb: return AttackEnum.Bomb;
                case SelectableTiles.Charge: return AttackEnum.Charge;
                case SelectableTiles.BackCharge: return AttackEnum.BackCharge;
                case SelectableTiles.ShadowDash: return AttackEnum.ShadowDash;
                case SelectableTiles.SmokeBomb: return AttackEnum.SmokeBomb;
                case SelectableTiles.BackSmokeBomb: return AttackEnum.BackSmokeBomb;
                case SelectableTiles.Nagiboku: return AttackEnum.Nagiboku;
                case SelectableTiles.Shuriken: return AttackEnum.Shuriken;
                case SelectableTiles.Chakram: return AttackEnum.Chakram;
                case SelectableTiles.Curse: return AttackEnum.Curse;
                case SelectableTiles.TurnAround: return AttackEnum.TurnAround;
                case SelectableTiles.EarthImpale: return AttackEnum.EarthImpale;
                case SelectableTiles.Mirror: return AttackEnum.Mirror;
                case SelectableTiles.BackStrike: return AttackEnum.BackStrike;
                case SelectableTiles.ShadowKama: return AttackEnum.ShadowKama;
                case SelectableTiles.Tetsubo: return AttackEnum.Tetsubo;
                case SelectableTiles.Kunai: return AttackEnum.Kunai;
                case SelectableTiles.BladeOfPatience: return AttackEnum.BladeOfPatience;
                case SelectableTiles.Dash: return AttackEnum.Dash;
                case SelectableTiles.Crossbow: return AttackEnum.Crossbow;
                case SelectableTiles.SwapToss: return AttackEnum.SwapToss;
                case SelectableTiles.OriginOfSymmetry: return AttackEnum.OriginOfSymmetry;
                case SelectableTiles.Tanegashima: return AttackEnum.Tanegashima;
                case SelectableTiles.CorruptedBarrage: return AttackEnum.CorruptedBarrage;
                case SelectableTiles.ScarStrike: return AttackEnum.ScarStrike;
                case SelectableTiles.SignatureMove: return AttackEnum.SignatureMove;
                case SelectableTiles.MeteorHammer: return AttackEnum.MeteorHammer;
                case SelectableTiles.Kaitenryuken: return AttackEnum.Kaitenryuken;
                case SelectableTiles.Barricade: return AttackEnum.Barricade;
                case SelectableTiles.BackShadowDash: return AttackEnum.BackShadowDash;
                case SelectableTiles.Sai: return AttackEnum.Sai;
                case SelectableTiles.BlazingSuisei: return AttackEnum.BlazingSuisei;
                case SelectableTiles.Mon: return AttackEnum.Mon;
                case SelectableTiles.Thorns: return AttackEnum.Thorns;
                case SelectableTiles.CorruptedWaveL2R: return AttackEnum.CorruptedWaveL2R;
                case SelectableTiles.CorruptedWaveR2L: return AttackEnum.CorruptedWaveR2L;
                case SelectableTiles.Hookblade: return AttackEnum.Hookblade;
                case SelectableTiles.BossSwap: return AttackEnum.BossSwap;
                default: return AttackEnum.Katana;
            }
        }

        private static int DamageToInt(Damage damage)
        {
            switch (damage)
            {
                case Damage.NegativeOne: return -1;
                case Damage.Zero: return 0;
                case Damage.One: return 1;
                case Damage.Two: return 2;
                case Damage.Three: return 3;
                case Damage.Four: return 4;
                case Damage.Five: return 5;
                case Damage.Six: return 6;
                case Damage.Seven: return 7;
                case Damage.Eight: return 8;
                case Damage.Nine: return 9;
                default: return -2;
            }
        }

        private static int CooldownToInt(Cooldown cooldown)
        {
            switch (cooldown)
            {
                case Cooldown.Zero: return 0;
                case Cooldown.One: return 1;
                case Cooldown.Two: return 2;
                case Cooldown.Three: return 3;
                case Cooldown.Four: return 4;
                case Cooldown.Five: return 5;
                case Cooldown.Six: return 6;
                case Cooldown.Seven: return 7;
                case Cooldown.Eight: return 8;
                default: return -1;
            }
        }

        public static bool IsDamagingTile(AttackEnum attack)
        {
            switch (attack)
            {
                case AttackEnum.Curse:
                case AttackEnum.TurnAround:
                case AttackEnum.Mirror:
                case AttackEnum.Dash:
                case AttackEnum.SwapToss:
                case AttackEnum.OriginOfSymmetry:
                case AttackEnum.SignatureMove:
                case AttackEnum.Barricade:
                case AttackEnum.BossSwap:
                    return false;
                default: return true;
            }
        }

        public static SkillEnum SkillIdToEnum(int skillId)
        {
            switch (skillId)
            {
                case 0: return SkillEnum.BackStabber;
                case 1: return SkillEnum.UnfriendlyFire;
                case 2: return SkillEnum.Sniper;
                case 3: return SkillEnum.Mindfulness;
                case 4: return SkillEnum.Monomancer;
                case 5: return SkillEnum.CloseCombat;
                case 6: return SkillEnum.CentralDominion;
                case 7: return SkillEnum.OddCurse;
                case 8: return SkillEnum.ComboCoin;
                case 9: return SkillEnum.TripleComboHeal;
                case 10: return SkillEnum.ComboRecharge;
                case 11: return SkillEnum.ComboCurse;
                case 12: return SkillEnum.ComboDeal;
                case 13: return SkillEnum.KobushiCombo;
                case 14: return SkillEnum.ComboBoon;
                case 15: return SkillEnum.ChillingCombo;
                case 16: return SkillEnum.Healthy;
                case 17: return SkillEnum.Fortress;
                case 18: return SkillEnum.ReactiveShield;
                case 19: return SkillEnum.ShieldRetention;
                case 20: return SkillEnum.Karma;
                case 21: return SkillEnum.ChillingBlood;
                case 22: return SkillEnum.IronSkin;
                case 23: return SkillEnum.OverflowGuard;
                case 24: return SkillEnum.TwoWayMove;
                case 25: return SkillEnum.QuickRecovery;
                case 26: return SkillEnum.DamagingMove;
                case 27: return SkillEnum.DynamicBoost;
                case 28: return SkillEnum.CursingMove;
                case 29: return SkillEnum.ChikaraCrush;
                case 30: return SkillEnum.MamushiMove;
                case 31: return SkillEnum.TwoFacedDancer;
                case 32: return SkillEnum.FenghuangFeather;
                case 33: return SkillEnum.SeiryuScale;
                case 34: return SkillEnum.BigPockets;
                case 35: return SkillEnum.RogueRetail;
                default: return SkillEnum.BackStabber;
            }
        }
    }
}
