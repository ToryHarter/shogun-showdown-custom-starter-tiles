﻿using TileEnums;
using UnityEngine;
using UnityModManagerNet;

namespace CustomStarterTiles
{
    [DrawFields(DrawFieldMask.Public)]
    public class TileSettings
    {
        [Draw(Label = "Tile:", Tooltip = "Tile:\n\nWhat tile should be in this deck position."), Space(5)]
        public SelectableTiles Tile = SelectableTiles.Katana;

        public AttackEnum AttackValue { get; set; }

        [Draw(Label = "Max Level:", Max = 8, Min = 0, Tooltip = "Max Level:\n\nThe number of unlocked levels for this tile. Default is 3."), Space(5)]
        public int Level = 3;

        [Draw(Label = "Damage:", Tooltip = "Damage:\n\nThe amount of damage this tile should do."), Space(5)]
        public Damage Damage = Damage.Default;

        public int DamageValue { get; set; }

        [Draw(Label = "Cooldown:", Tooltip = "Cooldown:\n\nTurn cooldown for this tile."), Space(5)]
        public Cooldown Cooldown = Cooldown.Default;

        public int CooldownValue { get; set; }

        [Draw(Label = "Enchantment:", Tooltip = "Enchantment:\n\nThe enchantment this tile should have."), Space(5)]
        public AttackEffectEnum Enchantment = AttackEffectEnum.None;

        [Draw(Label = "Free-Play:", Tooltip = "Free-Play:\n\nIf checked will make the tile not take a turn to add to the attack queue."), Space(5)]
        public bool IsFreePlay = false;
    }
}
