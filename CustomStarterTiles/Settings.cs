﻿using System;
using UnityEngine;
using UnityModManagerNet;

namespace CustomStarterTiles
{
    public class Settings : UnityModManager.ModSettings, IDrawable
    {
        [Draw(Label = "Deck Size", Max = 8, Min = 1, Tooltip = "Deck Size\n\nHow many tiles your starter deck will contain.\nDefault is 2.")]
        public int DeckSize = 2;

        [Draw(Label = "Randomized", Tooltip = "Randomized\n\nIf randomized, starter deck will be randomly generated at the start of each run.\nIf not randomized, starter deck will be replaced at the start of each run with the player specified tiles."), Space(10)]
        public bool IsRandomized = true;

        [Draw(Label = "Allow Duplicates", VisibleOn = "IsRandomized|True", Tooltip = "Allow Duplicates\n\nAllows the randomized starter deck to potentially contain the same tile."), Space(10)]
        public bool AllowDuplicates = false;

        [Draw(Label = "Force Damaging Tile", VisibleOn = "IsRandomized|True", Tooltip = "Force Damaging Tile\n\nRandomized starter decks will always contain a tile that does damage.\nRecommended to keep this true.")]
        public bool ForceDamagingTile = true;

        [Draw(Label = "Only Obtainable Tiles", VisibleOn = "IsRandomized|True", Tooltip = "Only Obtainable Tiles\n\nPrevents randomized starter decks from containing tiles that the player is not capable of obtaining in the vanilla game.")]
        public bool OnlyObtainableTiles = false;

        [Draw(Label = "Tile: 1", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 1:\n\nWhat tile should be in the 1st deck position."), Space(10)]
        public TileSettings Tile1 = new TileSettings();

        [Draw(Label = "Tile: 2", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 2:\n\nWhat tile should be in the 2nd deck position.")]
        public TileSettings Tile2 = new TileSettings();

        [Draw(Label = "Tile: 3", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 3:\n\nWhat tile should be in the 3rd deck position.")]
        public TileSettings Tile3 = new TileSettings();

        [Draw(Label = "Tile: 4", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 4:\n\nWhat tile should be in the 4th deck position.")]
        public TileSettings Tile4 = new TileSettings();

        [Draw(Label = "Tile: 5", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 5:\n\nWhat tile should be in the 5th deck position.")]
        public TileSettings Tile5 = new TileSettings();

        [Draw(Label = "Tile: 6", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 6:\n\nWhat tile should be in the 6th deck position.")]
        public TileSettings Tile6 = new TileSettings();

        [Draw(Label = "Tile: 7", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 7:\n\nWhat tile should be in the 7th deck position.")]
        public TileSettings Tile7 = new TileSettings();

        [Draw(Label = "Tile: 8", Collapsible = true, VisibleOn = "IsRandomized|False", Tooltip = "Tile 8:\n\nWhat tile should be in the 8th deck position.")]
        public TileSettings Tile8 = new TileSettings();

        [Draw(Label = "Skills", Collapsible = true, Min = 0, Max = 29, Tooltip = "Skills\n\nDue to limitations with UMM skills are represented with an ID #\nBackstabber: 0, Unfriendly Fire: 1, Sniper: 2,\nMindfulness: 3, Monomancer: 4, Close Combat: 5\nCentral Dominion: 6, Odd Curse: 7, Combo Coin: 8\nTriple Combo Heal: 9, Combo Recharge: 10, Combo Curse: 11\nCombo Deal: 12, Kobushi Combo: 13, Combo Boon: 14\nChillingCombo: 15, Healthy: 16, Fortress: 17\nReactive Shield: 18, Shield Retention: 19, Karma: 20\nChilling Blood: 21, Iron Skin: 22, Overflow Guard: 23\nTwo Way Move: 24, Quick Recovery: 25, Damaging Move: 26\nDynamic Boost: 27, Cursing Move: 28, Chikara Crush: 29\nMamushi Move: 30, Two Faced Dancer: 31, Fenghuang Feather: 32\nSeiryu Scale: 33, Big Pockets: 34, Rogue Retail: 35"), Space (10)]
        public int[] Skills = new int[0];

        public void OnChange() {}

        public override void Save(UnityModManager.ModEntry modEntry)
        {
            try
            {
                ModModule.Mod.Logger.Log("Saving UnityModManager settings.");

                Save(this, modEntry);

                ModModule.Mod.Logger.Log("Successfully saved UnityModManager settings.");
            }
            catch (Exception e)
            {
                ModModule.Mod.Logger.Error($"Error saving UnityModManager Settings: {e.InnerException}");
            }
        }
    }
}
